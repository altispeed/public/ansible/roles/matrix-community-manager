---
- name: Check if configuration.toml exists
  stat:
    path: "{{ mcm_base_path }}/Skill/configuration.toml"
  register: configtoml
  tags:
    - setup-all
    - setup-mcm

- name: Backup configuration.toml file
  ansible.builtin.copy:
    src: "{{ mcm_base_path }}/Skill/configuration.toml"
    dest: "{{ mcm_base_path }}/Skill/configuration.toml.bak"
    remote_src: true
  when: configtoml.stat.exists
  become: True
  tags:
    - setup-all
    - setup-mcm

- name: Install git
  package:
    name: git
    state: present
  become: true
  tags:
    - setup-all
    - setup-mcm

- name: Create project folder
  file:
      path: "{{ mcm_base_path }}"
      state: directory
      owner: "root"
      group: "root"
      mode: 0770
  become: true
  tags:
    - setup-all
    - setup-mcm
    - update

- name: Clone MCM repo
  git:
    repo: https://gitlab.com/Sleuth56/Matrix-Community-Manager.git
    dest: "{{ mcm_base_path }}"
  become: true
  tags:
    - setup-all
    - setup-mcm
    - update

- name: Obtaining a device ID by logging into the supplied matrix account
  uri:
    url: "{{ mcm_homeserver }}/_matrix/client/v3/login"
    method: POST
    headers: 
      Content-Type: application/json
    body_format: json
    body: '{"identifier":{"type":"m.id.user","user":"{{ mcm_mxid | regex_findall("@(.*):.*", "\\1") | first }}"},"initial_device_display_name":"mcm","password":"{{ mcm_password }}","type":"m.login.password"}'
  register: device_id
  when: not mcm_deviceid
  become: true
  tags:
    - setup-all
    - setup-mcm
    - fix-mcm
    - deviceid-mcm

- name: Remove encryption cache
  ansible.builtin.file:
    path: "{{ mcm_base_path }}/Matrix_cache"
    state: absent
  become: true
  tags:
    - setup-all
    - setup-mcm
    - fix-mcm

- name: Print debug info
  debug:
    msg: 'DeviceID: {{ device_id["json"]["device_id"] }}'
  when: not mcm_deviceid
  tags:
    - setup-all
    - setup-mcm
    - fix-mcm
    - deviceid-mcm

- name: Re-build configuration files
  include_tasks: redo_config.yml
  tags:
    - setup-all
    - setup-mcm
    - reconfigure-mcm

- name: Stop container
  community.docker.docker_compose:
    project_src: "{{ mcm_base_path }}/"
    state: absent
  become: true
  tags:
    - setup-all
    - setup-mcm
    - stop

- name: Start container
  community.docker.docker_compose:
    project_src: "{{ mcm_base_path }}/"
    state: present
  become: true
  tags:
    - setup-all
    - setup-mcm
    - start

- name: Wait for sync message
  debug:
    msg: "Allow a minute for initial sync before interacting with the bot"
  tags:
    - setup-all
    - setup-mcm
    - start