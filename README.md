# matrix-community-manager

Deploy and Configure Matrix Community Manager Bot

## Table of content

- [Default Variables](#default-variables)
  - [mcm_admin_mxid](#mcm_admin_mxid)
  - [mcm_admin_room](#mcm_admin_room)
  - [mcm_announcement](#mcm_announcement)
  - [mcm_auto_join](#mcm_auto_join)
  - [mcm_autoreply](#mcm_autoreply)
  - [mcm_base_path](#mcm_base_path)
  - [mcm_device_name](#mcm_device_name)
  - [mcm_enable_encryption](#mcm_enable_encryption)
  - [mcm_filter_delimiter](#mcm_filter_delimiter)
  - [mcm_filter_require_mention](#mcm_filter_require_mention)
  - [mcm_filter_sucess_response](#mcm_filter_sucess_response)
  - [mcm_filters](#mcm_filters)
  - [mcm_homeserver](#mcm_homeserver)
  - [mcm_JoinOnlyAdminInvites](#mcm_JoinOnlyAdminInvites)
  - [mcm_mxid](#mcm_mxid)
  - [mcm_nick](#mcm_nick)
  - [mcm_OnlyInvitesFromTrustedServers](#mcm_OnlyInvitesFromTrustedServers)
  - [mcm_OnlyRoomsOnTrustedServers](#mcm_OnlyRoomsOnTrustedServers)
  - [mcm_output_format](#mcm_output_format)
  - [mcm_password](#mcm_password)
  - [mcm_require_mention](#mcm_require_mention)
  - [mcm_success_response](#mcm_success_response)
  - [mcm_time_zone](#mcm_time_zone)
  - [mcm_TrustedServers](#mcm_TrustedServers)
- [Discovered Tags](#discovered-tags)
- [Dependencies](#dependencies)
- [License](#license)
- [Author](#author)

---

## Default Variables

### mcm_admin_mxid

#### Default value

```YAML
mcm_admin_mxid: '@example:example.com'
```

### mcm_admin_room

#### Default value

```YAML
mcm_admin_room: '!XXXXXXXXXXXXXXXXX:example.com'
```

### mcm_announcement

#### Default value

```YAML
mcm_announcement: []
```

### mcm_auto_join

#### Default value

```YAML
mcm_auto_join: 'true'
```

### mcm_autoreply

#### Default value

```YAML
mcm_autoreply: []
```

### mcm_base_path

#### Default value

```YAML
mcm_base_path: /matrix-community-manager
```

### mcm_device_name

#### Default value

```YAML
mcm_device_name: mcm
```

### mcm_enable_encryption

#### Default value

```YAML
mcm_enable_encryption: 'True'
```

### mcm_filter_delimiter

#### Default value

```YAML
mcm_filter_delimiter: '#'
```

### mcm_filter_require_mention

#### Default value

```YAML
mcm_filter_require_mention: 'false'
```

### mcm_filter_sucess_response

#### Default value

```YAML
mcm_filter_sucess_response: Page sent!
```

### mcm_filters

#### Default value

```YAML
mcm_filters: []
```

### mcm_homeserver

#### Default value

```YAML
mcm_homeserver: https://matrix.example.com
```

### mcm_JoinOnlyAdminInvites

#### Default value

```YAML
mcm_JoinOnlyAdminInvites: 'true'
```

### mcm_mxid

#### Default value

```YAML
mcm_mxid: '@example:example.com'
```

### mcm_nick

#### Default value

```YAML
mcm_nick: Bravo System
```

### mcm_OnlyInvitesFromTrustedServers

#### Default value

```YAML
mcm_OnlyInvitesFromTrustedServers: 'true'
```

### mcm_OnlyRoomsOnTrustedServers

#### Default value

```YAML
mcm_OnlyRoomsOnTrustedServers: 'false'
```

### mcm_output_format

#### Default value

```YAML
mcm_output_format: <h4>Page from {user_name} {from_room}</h4><blockquote><p>{message}</p></blockquote>
```

### mcm_password

#### Default value

```YAML
mcm_password: changem3
```

### mcm_require_mention

#### Default value

```YAML
mcm_require_mention: 'true'
```

### mcm_success_response

#### Default value

```YAML
mcm_success_response: Page sent!
```

### mcm_time_zone

#### Default value

```YAML
mcm_time_zone: US/Central
```

### mcm_TrustedServers

#### Default value

```YAML
mcm_TrustedServers: []
```

## Discovered Tags

**_deviceid-mcm_**

**_fix-mcm_**

**_reconfigure-mcm_**

**_setup-all_**

**_setup-mcm_**

**_start_**

**_stop_**

**_update_**


## Dependencies

None.

## License

GPL-3.0-only

## Author

Sleuth and Dark Decoy
